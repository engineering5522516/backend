SHELL := /bin/bash

py_warn = PYTHONDEVMODE=1

code_dir = backend/
tests_dir = tests/
test_modules = $(code_dir)/biz,$(code_dir)/endpoints,

CMD:=python3 -m


check:
	$(CMD) flake8 $(code_dir); \
	$(CMD) isort -src --check-only $(code_dir) $(tests_dir); \
	$(CMD) black --check $(code_dir) $(tests_dir); \
	$(CMD) xenon -b B -m B -a B $(code_dir);

format:
	$(CMD) isort -src $(code_dir) $(tests_dir); \
	$(CMD) black $(code_dir);

test:
	$(CMD) pytest;

#	$(CMD) coverage run --source=$(test_modules) -p -m pytest $(tests_dir); \
#	$(CMD) coverage combine; \
#	$(CMD) coverage report; \
#	$(CMD) coverage html;
