"""
    FastAPI application
"""

import os

import config
from fastapi import FastAPI

from .endpoints import router

config = config.Config()


def _build_routers(app: FastAPI) -> None:
    app.include_router(router)


def create_app() -> FastAPI:
    os.makedirs(config.base_dir / config.image_dir_name, exist_ok=True)
    app: FastAPI = FastAPI()
    _build_routers(app)
    return app
