"""
    Project enums
"""

from enum import Enum

from celery.states import REJECTED  # noqa
from celery.states import FAILURE, IGNORED, PENDING, RECEIVED, RETRY, REVOKED, STARTED, SUCCESS


class CeleryStates(str, Enum):
    """
    Set of all possible task states:
    PENDING - Task is waiting for execution or unknown
    RECEIVED - Task was received by a worker
    STARTED - Task was started by a worker
    SUCCESS - Task has been successfully executed
    FAILURE - Task execution resulted in failure
    REVOKED - Task has been revoked
    REJECTED - Task was rejected
    RETRY - Task is being retried
    IGNORED - Task was ignored
    """

    PENDING = PENDING
    RECEIVED = RECEIVED
    STARTED = STARTED
    SUCCESS = SUCCESS
    FAILURE = FAILURE
    REVOKED = REVOKED
    REJECTED = REJECTED
    RETRY = RETRY
    IGNORED = IGNORED
