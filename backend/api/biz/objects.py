"""
    `business` objects
"""

import uuid
from typing import Union

import config
from celery import Celery
from celery.result import AsyncResult
from celery.states import PENDING
from fastapi import Response, UploadFile, status
from pydantic import UUID4

from .enums import CeleryStates

config = config.Config()


class PredictObj:
    """Unique identifier is assigned to the file, after which the task is created and started with the status PENDING"""

    def __call__(
        self, file: UploadFile, file_extension: str, celery_app: Celery
    ) -> dict[str, Union[UUID4, str]]:
        _id = str(uuid.uuid4())
        image_name = f"{_id}{file_extension}"
        image_path = config.base_dir / config.image_dir_name / image_name
        with open(image_path, "wb") as f:
            f.write(file.file.read())

        celery_app.send_task("call_model", kwargs={"image_name": image_name}, task_id=_id)
        return {"id": _id, "status": PENDING}


class RetrieveObj:
    """
    Обработка результатов задачи и вывод её статуса
    """

    def __call__(
        self, response: Response, task_result: AsyncResult
    ) -> dict[str, Union[CeleryStates, str, None]]:
        if not task_result.ready():
            response.status_code = status.HTTP_201_CREATED
            return {"status": task_result.state}
        return {"result": task_result.get(), "status": task_result.state}
