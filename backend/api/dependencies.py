"""
    FastAPi reusable dependencies
"""
import os
from typing import Generator, NoReturn, TypeVar

import config
from celery import Celery
from celery.result import AsyncResult
from celery.states import PENDING
from fastapi import Depends, HTTPException, Request, UploadFile, status
from pydantic import UUID4
from redis import ConnectionPool, Redis

config = config.Config()
biz_T = TypeVar("biz_T")


class ValidateExtOfUploadFile:
    """Check the file extension against the list of available extensions"""

    extensions: list[str]

    def __init__(self, extensions: list[str]) -> None:
        for i in extensions:
            if not i.startswith("."):
                raise ValueError("All extension in extensions should start with `.`")
            self.extensions = extensions

    def __call__(self, file: UploadFile) -> NoReturn:
        if not any([file.filename.endswith(i) for i in self.extensions]):
            raise HTTPException(
                status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
                detail=f"Only images with extension {' '.join(self.extensions)}",
            )


class BizObjectFactory:
    """Simple businnes objects factory"""

    biz: biz_T

    def __init__(self, biz: type[biz_T], *args, **kwargs) -> None:
        self.biz = biz(*args, **kwargs)

    def __call__(self) -> biz_T:
        return self.biz


pool: ConnectionPool = ConnectionPool(
    host=os.getenv("redis_host") or config.redis_host,
    port=config.redis_port,
    db=config.redis_db,
    password=config.redis_password,
)


def get_redis() -> Generator[Redis, None, None]:
    """Redis instance generator"""
    redis: Redis = Redis(connection_pool=pool)
    yield redis
    redis.close()


def get_celery() -> Generator[Celery, None, None]:
    """Celery instance generator"""
    celery_app = Celery(__name__)
    celery_app.config_from_object(config.celery_config_path)
    yield celery_app
    celery_app.close()


def get_task_by_id(uuid: UUID4, cel: Celery = Depends(get_celery)) -> AsyncResult:
    """
    Получает результат выполнения задачи по указанному идентификатору.

    :param uuid:  UUID4 идентификатор задачи.
    :param cel: Объект Celery для получения результата.
    :return: Результат выполнения задачи (AsyncResult).
    """
    result = AsyncResult(str(uuid), app=cel)
    if str(result.state) == PENDING:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Task doesn't exists")
    return result


def request_delay(request: Request, redis: Redis = Depends(get_redis)) -> None:
    """
    Adds rate limiting to the request based on the client's IP address using Redis.

    Parameters:
    :param request : The incoming request object.
    :param redis : The Redis client used to check if an IP has made too many requests.
    :return None: Returns nothing, but adds rate limiting for the incoming request.
    """
    ip = request.client.host
    if redis.exists(ip):
        raise HTTPException(
            status_code=status.HTTP_429_TOO_MANY_REQUESTS, detail="Too many requests"
        )
    redis.setex(ip, config.time_delay, ip)


def get_file_ext(file: UploadFile) -> str:
    """Return file extension from `file.filename`"""
    _, extension = os.path.splitext(file.filename)
    return extension
