"""
    Endpoints
"""
from typing import Any

from celery import Celery
from celery.result import AsyncResult
from fastapi import APIRouter, Depends, Response, UploadFile

from ..biz import PredictObj, RetrieveObj
from ..dependencies import (
    BizObjectFactory,
    ValidateExtOfUploadFile,
    get_celery,
    get_file_ext,
    get_task_by_id,
    request_delay,
)
from .schemas import ResponsePredict, ResponseRetrieve

router: APIRouter = APIRouter()


# FastAPI endpoint handlers don't necessarily have a declared return type, it doesn't make sense
# see: https://github.com/tiangolo/fastapi/issues/101#issuecomment-475994050


@router.post(
    "/predict",
    response_model=ResponsePredict,
    dependencies=[Depends(request_delay), Depends(ValidateExtOfUploadFile([".jpeg", ".jpg"]))],
    responses={
        200: {"description": "Запрос успешно обработан"},
    },
)
def predict(
    file: UploadFile,
    file_extension: str = Depends(get_file_ext),
    celery_app: Celery = Depends(get_celery),
    biz_obj: PredictObj = Depends(BizObjectFactory(PredictObj)),
) -> Any:
    """Endpoint loads the image, checks the file extension, and starts the task"""
    return biz_obj(file, file_extension, celery_app)


@router.get(
    "/retrieve/{uuid}",
    response_model=ResponseRetrieve,
    dependencies=[Depends(request_delay)],
    responses={
        200: {"description": "Запрос успешно обработан"},
        201: {"description": "Ваш запрос обрабатывается"},
    },
)
def retrieve(
    response: Response,
    task_result: AsyncResult = Depends(get_task_by_id),
    biz_obj: RetrieveObj = Depends(BizObjectFactory(RetrieveObj)),
) -> Any:
    """
    Обработка запроса на поиск задачи по ее ID.
    """
    return biz_obj(response, task_result)
