"""
    Enpoints schemas
"""

from typing import Optional

from pydantic import UUID4, BaseModel

from ..biz import CeleryStates


class ResponsePredict(BaseModel):
    id: UUID4
    status: CeleryStates


class ResponseRetrieve(BaseModel):
    status: CeleryStates
    result: Optional[str]
