"""
    Project configuration
"""
from pathlib import Path

from pydantic import BaseSettings


class Config(BaseSettings):
    """
    Configuration class
    """

    base_dir: Path = Path(__file__).parent.parent
    image_dir_name: str = "images"
    image_dir: Path = base_dir / image_dir_name
    time_delay: int = 15

    host: str = "127.0.0.1"
    port: int = 5000
    reload: bool = True

    redis_host: str = "127.0.0.1"
    redis_port: int = 6379
    redis_db: int = 0
    redis_password: str = "password"

    celery_argv: list[str] = [
        "worker",
        "--loglevel=DEBUG",
    ]
    celery_config_path: str = "backend.worker.celeryconfig"

    class Config:
        env_prefix = ""
        env_file = ".env"
        env_file_encoding = "utf-8"


config: Config = Config()
