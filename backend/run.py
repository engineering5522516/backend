"""
    Entrypoint & cli
"""

import importlib
import platform
from typing import Literal, NoReturn, Optional

import config
import typer

config = config.Config()
loop: Literal["uvloop", "asyncio"]
try:
    import uvloop  # noqa: F401
except ImportError:
    loop = "asyncio"
else:
    loop = "uvloop"


app: typer.Typer = typer.Typer()


def _run_uvicorn() -> NoReturn:
    """Run FatsAPI application over uvicorn server"""
    import uvicorn

    # module = importlib.import_module("config")
    # config = getattr(module, "config")

    uvicorn.run(
        "api.app:create_app",
        host=config.host,
        port=config.port,
        reload=config.reload,
        loop=loop,
        factory=True,
    )


def _run_celery() -> NoReturn:
    """Run Celery application"""
    argv = config.celery_argv
    if platform.system() == "Windows":
        argv.append("--pool=solo")

    module = importlib.import_module("worker.app")
    celery_app = module.create_app()
    celery_app.worker_main(argv)


@app.command()
def main(worker: Optional[bool] = typer.Option(False, "--worker")) -> NoReturn:
    """cli entrypoint"""
    if worker:
        _run_celery()
    else:
        _run_uvicorn()


if __name__ == "__main__":
    app()
