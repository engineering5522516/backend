"""
    Celery application
"""

from celery import Celery

from . import celeryconfig
from .tasks import PredictTask, call_model


def create_app() -> Celery:
    """Build celery app"""
    app = Celery(__name__)
    app.config_from_object(celeryconfig)

    app.task(call_model, name="call_model", ignore_result=False, bind=True, base=PredictTask)

    return app
