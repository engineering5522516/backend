"""
    Celery configuration file
    http://docs.celeryproject.org/en/latest/configuration.html
"""
import os

import config

config = config.Config()

_redis_host = os.getenv("redis_host") or config.redis_host
BROKER_URL: str = (
    f"redis://:{config.redis_password}@{_redis_host}:{config.redis_port}/{config.redis_db}"
)
CELERY_RESULT_BACKEND: str = BROKER_URL

# json serializer is more secure than the default pickle
CELERY_TASK_SERIALIZER: str = "json"
CELERY_ACCEPT_CONTENT: list[str] = ["json"]
CELERY_RESULT_SERIALIZER: str = "json"

# track task states
CELERY_TASK_TRACK_STARTED: bool = True

# Use UTC instead of localtime
CELERY_ENABLE_UTC: bool = True
