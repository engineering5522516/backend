"""
    Celery tasks
"""

from typing import Any, Optional

import config
from celery import Task
from PIL import Image
from transformers import ViTForImageClassification, ViTImageProcessor

config = config.Config()


class PredictTask(Task):
    """
    Class for load ml model once and cache it
    """

    abstract = True
    processor: Optional[ViTImageProcessor]
    model: Optional[ViTForImageClassification]

    def __init__(self) -> None:
        super().__init__()
        self.processor = None
        self.model = None

    def __call__(self, *args, **kwargs) -> Any:
        """
        Load model on first call (i.e. first task processed)
        Avoids the need to load model on each task request
        """
        if any([not self.processor, not self.model]):
            self.processor = ViTImageProcessor.from_pretrained("google/vit-base-patch16-224")
            self.model = ViTForImageClassification.from_pretrained("google/vit-base-patch16-224")

        return self.run(*args, **kwargs)


def call_model(self: PredictTask, image_name: str) -> str:
    image = Image.open(config.image_dir / image_name)
    inputs = self.processor(images=image, return_tensors="pt")
    outputs = self.model(**inputs)
    logits = outputs.logits
    predicted_class_idx = logits.argmax(-1).item()
    return self.model.config.id2label[predicted_class_idx]
