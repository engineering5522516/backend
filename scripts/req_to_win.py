import pathlib
from typing import Final
from pip._internal.operations import freeze  # noqa


_skip_deps: Final[list[str]] = ["uvloop", "-e git"]


def make_req_unpinned():
    base_dir: pathlib.Path = pathlib.Path(__file__).parent.parent.resolve()
    requirements: list[str] = list(freeze.freeze())
    new_requirements = []
    for req in requirements:
        for skip in _skip_deps:
            if skip in req:
                break
        else:
            r = req.split("==")[0]
            new_requirements.append(r)

    with open(base_dir / "requirements.txt", "w") as f:
        f.write("\n".join(new_requirements))


