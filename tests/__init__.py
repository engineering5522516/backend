import pathlib
import sys

_path = pathlib.Path(__file__).parent.parent.resolve() / "backend"
sys.path.append(str(_path))
