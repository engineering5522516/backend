import os
import shutil

import pytest
from config import config
from fastapi import FastAPI

from backend.api.app import create_app


@pytest.fixture(scope="session")
def app() -> FastAPI:
    app: FastAPI = create_app()
    yield app
    del app


@pytest.fixture(scope="function")
def image_dir():
    os.makedirs(config.base_dir / config.image_dir_name, exist_ok=True)
    yield
    shutil.rmtree(config.base_dir / config.image_dir_name, ignore_errors=True)
