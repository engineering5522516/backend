from fastapi import FastAPI


def test_app_is_fastapi_app(app: FastAPI):
    """stub test check app is FastAPI instance"""
    assert isinstance(app, FastAPI)
