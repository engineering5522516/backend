import os
from unittest import mock

import pytest
from celery.states import PENDING
from fastapi import HTTPException, status

from backend.api.biz.objects import PredictObj
from backend.api.dependencies import ValidateExtOfUploadFile


@pytest.fixture
def mock_celery():
    celery_app = mock.MagicMock()
    return celery_app


class UploadFile:
    def __init__(self, filename: str, file):
        self.filename = filename
        self.file = file


def test_predict_obj(mock_celery, image_dir):
    with open("test_file.jpg", "wb") as f:
        f.write(b"test")
    file = open("test_file.jpg", "rb")
    upload_file = UploadFile(filename="test_file.jpg", file=file)
    predict = PredictObj()

    result = predict(upload_file, ".jpg", mock_celery)
    file.close()
    os.remove("test_file.jpg")
    assert isinstance(result, dict)
    assert "id" in result
    assert "status" in result
    assert result["status"] == PENDING


def test_validate_ext_of_upload_file():
    extensions = [".jpg", ".jpeg", ".png"]
    validator = ValidateExtOfUploadFile(extensions)

    with open("test_file.jpg", "wb") as f:
        f.write(b"test")
    file = open("test_file.jpg", "rb")
    upload_file = UploadFile(filename="test_file.jpg", file=file)
    assert validator(upload_file) is None

    with open("test_file.txt", "wb") as f:
        f.write(b"test")
    file = open("test_file.txt", "rb")
    upload_file = UploadFile(filename="test_file.txt", file=file)
    os.remove("test_file.txt")
    os.remove("test_file.jpg")
    with pytest.raises(HTTPException) as exc_info:
        validator(upload_file)
    assert exc_info.value.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert "Only images with extension" in exc_info.value.detail
    assert all([ext in exc_info.value.detail for ext in extensions])
