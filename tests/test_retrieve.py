from celery.states import RECEIVED

from backend.api.biz.objects import RetrieveObj


class Response:
    def __init__(self):
        self.status_code = None


class AsyncResult:
    def __init__(self, value: bool):
        self.value = value
        self.state = RECEIVED

    def ready(self) -> bool:
        return self.value

    def get(self) -> str:
        return "Ok"


def test_retrieve_obj():
    retrieve_object = RetrieveObj()
    response = Response()
    task_result = AsyncResult(value=True)
    result = retrieve_object(response, task_result)
    assert isinstance(result, dict)
    assert "result" in result
    assert "status" in result
    assert result["status"] == RECEIVED
    assert response.status_code is None

    task_result = AsyncResult(value=False)
    result = retrieve_object(response, task_result)
    assert isinstance(result, dict)
    assert "status" in result
    assert result["status"] == RECEIVED
    assert response.status_code == 201
